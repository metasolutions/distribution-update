const signin = require('./utils/signin');
const { getAPIDistribution, addFileToAPI } = require('./utils/api');
const fs = require('fs');
const logger = require('./utils/logger');

const {  
  getRepositoryViaResourceURI,
  setCookie,
  extractParameters,
  checkFile,
  getFormat,
  setFileLabel,
} = require('./utils/util');

const fileAdder = async (catalogId, distributionId, filePath, format, filename, entrystore, prototypeId) => {

  if(!entrystore) entrystore = await signin();
  const baseURI = entrystore.getBaseURI();
  logger.info(`Signed in to ${baseURI}`);
  const distribution = await entrystore.getEntry(entrystore.getEntryURI(catalogId, distributionId));
  logger.info(`Tried getting distribution of ${catalogId} and ${distributionId}`);
  const apiEntry = await getAPIDistribution(distribution);
  if (apiEntry && format!=='text/csv') throw new Error('Distribution has connected API, file has to be CSV formatted!');
  logger.info('Tried getting api distribution');

  let pe;
  if (prototypeId) pe = distribution.getContext().newEntry(prototypeId);
  else pe = distribution.getContext().newEntry();

  if (filename) {
    pe.addL('dcterms:title', filename);
  }
  pe.add("http://www.w3.org/1999/02/22-rdf-syntax-ns#type", 'http://entryscape.com/terms/File');
  logger.info('Uploading the file');
  const fileEntry = await pe.commit();
  const fileResource = await fileEntry.getResource();

  await fileResource.putFile(fs.createReadStream(filePath), format);

  if (filename) await setFileLabel(fileEntry, filename);

  logger.info('Updating the distribution with a link to the file and a new modification date.');
  const ruri = distribution.getResourceURI();
  const md = distribution.getMetadata();
  md.add(ruri, 'dcat:accessURL', fileEntry.getResourceURI());
  md.add(ruri, 'dcat:downloadURL', fileEntry.getResourceURI());
  md.findAndRemove(ruri, 'dcterms:modified');
  md.addD(ruri, 'dcterms:modified', new Date().toISOString(), 'xsd:dateTime');
  await distribution.commitMetadata();

  if (apiEntry) {
    logger.info('Adding the file to the connected API');
    await addFileToAPI(apiEntry, fileEntry);
  } else {
    logger.info('No connected API to update');
  }
  return fileEntry.getResourceURI();
}


if (process.argv.length > 2 && process.argv.length <=6) {
  const catalogId = process.argv[2];
  const distributionId = process.argv[3];
  const filePath = process.argv[4];
  const formatArg = process.argv[5];

  checkFile(filePath)
  const dotLocation = filePath.lastIndexOf('.');
  const filename = dotLocation !== -1 ? filePath.substring(0, dotLocation) : filePath;
  const extention = dotLocation !== -1 ? filePath.substring(dotLocation+1) : undefined;
  const format = formatArg ? formatArg
    : (extention && extention.toLowerCase() === 'csv' ? 'text/csv' : 'text/plain');
  fileAdder(catalogId, distributionId, filePath, format, filename, null);
} else if (process.argv.length < 2) {
  logger.info("This command allows you to see add a file to a distribution if there is a connected API it will be updated as well.");
  logger.info("$> node addFile.js catalogId distributionId pathToFile [format]");
  process.exit(1);
};

module.exports = async (cookie, filePath, filename, resourceURI, prototypeId) => {

  checkFile(filePath)
  const repository = getRepositoryViaResourceURI(resourceURI);
  const entrystore = setCookie(repository, cookie);
  const { entryId, contextId } = await extractParameters(entrystore, resourceURI, 'add');
  const format = getFormat(filename);
  logger.info(format);
  return fileAdder(contextId, entryId, filePath, format, filename, entrystore, prototypeId);
};