const signin = require('./utils/signin');
const { EntryStoreUtil } = require('@entryscape/entrystore-js');

const logger = require('./utils/logger');


if (process.argv.length < 3 || process.argv[2].indexOf('help') !== -1) {
  logger.info("This command allows you to list datasets or distributions in a catalog.");
  logger.info("If you provide only the catalog id datasets will be listed,");
  logger.info("if you also provide a datasets id its distributions will be listed instead.\n\n");
  logger.info("$> node list.js catalogId [datasetId]");
  logger.info("\nWhere catalogId is the contextId and datasetId is the entryId in EntryScape.");
  process.exit(1);
}
const catalogId = process.argv[2];
const datasetId = process.argv[3];

signin().then((es) => {
  const esu = new EntryStoreUtil(es);

  if (datasetId) {
    logger.info(`\nDistributions in dataset ${datasetId} within catalog ${catalogId}`);
    logger.info(`ID\tDistribution title / format / accessURL`);
    logger.info(`----------------------------------`);
    es.getEntry(es.getEntryURI(catalogId, datasetId)).then(dataset => {
      const distributionURIs = dataset.getMetadata().find(dataset.getResourceURI(), 'dcat:distribution').map(stmt => stmt.getValue());
      esu.loadEntriesByResourceURIs(distributionURIs, catalogId).then(distributions => {
        distributions.forEach(distribution => {
          const ruri = distribution.getResourceURI();
          const md = distribution.getMetadata();
          const title = md.findFirstValue(ruri, 'dcterms:title') || '';
          const format = md.findFirstValue(ruri, 'dcterms:format') || '';
          const access = md.findFirstValue(ruri, 'dcat:accessURL') || '';
          logger.info(`${distribution.getId()}\t${title}    ${format}    ${access}`);
        });
      })
    });
  } else {
    logger.info(`\nDatasets in catalog with id: ${catalogId}`);
    logger.info(`ID\tDataset title`);
    logger.info(`----------------------------------`);
    es.newSolrQuery().context(catalogId).rdfType('dcat:Dataset').forEach(dataset => {
      logger.info(`${dataset.getId()}\t${dataset.getMetadata().findFirstValue(dataset.getResourceURI(), 'dcterms:title')}`);
    });
  }
});