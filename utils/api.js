const { EntryStoreUtil, promiseUtil } = require('@entryscape/entrystore-js');
const { checkStatusOnRepeat } = require('./status');


const getAPIDistribution = async (sourceDistribution) => {
  const es = sourceDistribution.getEntryStore();
  const ruri = sourceDistribution.getResourceURI();
  const apiEntries = await es.newSolrQuery().uriProperty('dcterms:source', ruri).getEntries();
  return apiEntries.length > 0 ? apiEntries[0] : undefined;
}

const getSourceDistribution = (apiDistribution) => {
  const es = apiDistribution.getEntryStore();
  const sourceURI = apiDistribution.getMetadata().findFirstValue(apiDistribution.getResourceURI(), 'dcterms:source');
  return new EntryStoreUtil(es).getEntryByResourceURI(sourceURI, apiDistribution.getContext());
}

const getPipelineResource = context => context
  .getEntryById('rowstorePipeline')
  .then((pipeline) => pipeline.getResource());

/**
 * Updates the pipeline in two ways:
 * 1. Point to the right endpoint (discovered from the apiDistribution)
 * 2. Use the right action (e.g. replace or append)
 *
 * @param apiDistribution the entry corresponding to the distribution pointing to the API
 * @param pipeline the pipeline is the resource of the pipelineEntry
 * @param action replace or append
 */
const updatePipeline = (apiDistribution, pipeline, action) => {
  const datasetURL = apiDistribution.getMetadata().findFirstValue(null, 'dcat:accessURL');
  const transformId = pipeline.getTransformForType(pipeline.transformTypes.ROWSTORE);

  pipeline.setTransformArguments(transformId, {});
  pipeline.setTransformArguments(transformId, { action, datasetURL });
  return pipeline.commit();
}

const executePipeline = async (pipeline, fileEntry) => {
  const pipelineResultURIs = await pipeline.execute(fileEntry, {});
  const pipelineEntry = await pipeline.getEntryStore().getEntry(pipelineResultURIs[0]);
  return checkStatusOnRepeat(pipelineEntry);
}

const addFileToAPI = async (apiDistribution, fileEntry) => {
  const pipeline = await getPipelineResource(apiDistribution.getContext());
  await updatePipeline(apiDistribution, pipeline, 'append');
  await executePipeline(pipeline, fileEntry);
}

const getFiles = (sourceDistribution)  => {
  const es = sourceDistribution.getEntryStore();
  const esu = new EntryStoreUtil(es);
  const fileEntryResourceURIs = sourceDistribution.getMetadata()
    .find(sourceDistribution.getResourceURI(), 'dcat:downloadURL')
    .map(stmt => stmt.getValue());
  return esu.loadEntriesByResourceURIs(fileEntryResourceURIs, sourceDistribution.getContext());
}

const refreshAPI = async (apiDistribution, sourceDistribution) => {
  let _sourceDistribution = sourceDistribution;
  if (!sourceDistribution) {
    _sourceDistribution = await getSourceDistribution(apiDistribution);
  }
  const fileEntries = await getFiles(_sourceDistribution);

  if (fileEntries.length > 0) {
    const pipeline = await getPipelineResource(apiDistribution.getContext());
    await updatePipeline(apiDistribution, pipeline, 'replace');
    await executePipeline(pipeline, fileEntries.shift());

    if (fileEntries.length > 0) {
      await updatePipeline(apiDistribution, pipeline, 'append');
      await promiseUtil.forEach(fileEntries, (fileEntry) => {
        return executePipeline(pipeline, fileEntry);
      });
    }
  }
}

module.exports = {
  getAPIDistribution,
  addFileToAPI,
  refreshAPI,
}