const { EntryStore } = require('@entryscape/entrystore-js');

const logger = require('./logger');
const config = require('../config');

module.exports = async () => {

  const repository =  config.repository;
  const user = config.user;
  const password = config.password;


  const es = new EntryStore(repository);
  try {
    await es.getAuth().login(user, password);
    logger.info(`Signed in as ${user} to ${repository}`);
  } catch (err) {
    logger.error(err);
    logger.error(`Could not sign in as ${user} to ${repository}`);
    process.exit(1);
  }
  return es;
}