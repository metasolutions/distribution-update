const { promiseUtil } = require('@entryscape/entrystore-js');

/**
 * Get entry from EntryStore and not cache
 * @param {store/Entry} entry
 * @private
 */
const refreshEntry = (entry) => {
  entry.setRefreshNeeded();
  return entry.refresh();
};

/**
 * Update the cached external metadata of the entry from given data object
 *
 * @param {store/Entry} pipelineEntry
 * @param {object} data
 * @return {Promise}
 */
const update = async (pipelineEntry, data) => {
  // get a fresh copy of the entry
  await refreshEntry(pipelineEntry);

  const newStatus = id2status[data.status];
  const cachedExternalMetadata = pipelineEntry.getCachedExternalMetadata();
  const resourceURI = pipelineEntry.getResourceURI();

  // update status
  cachedExternalMetadata.findAndRemove(resourceURI, 'http://entrystore.org/terms/pipelineResultStatus');
  cachedExternalMetadata.addL(resourceURI, 'http://entrystore.org/terms/pipelineResultStatus', newStatus);

  // update columns
  cachedExternalMetadata.findAndRemove(resourceURI, 'http://entrystore.org/terms/pipelineResultColumnName');
  if (newStatus === 'available' && data.columnnames) {
    data.columnnames.forEach((col) => cachedExternalMetadata.addL(resourceURI, 'http://entrystore.org/terms/pipelineResultColumnName', col));
  }
  // update aliases
  cachedExternalMetadata.findAndRemove(resourceURI, 'http://entrystore.org/terms/aliasName');
  if (data.aliases && data.aliases.length > 0) {
    cachedExternalMetadata.addL(resourceURI, 'http://entrystore.org/terms/aliasName', data.aliases[0]);
  }

  return pipelineEntry.commitCachedExternalMetadata();
};

/**
 * Load the RowStore data for this entry
 * @param {store/Entry} pipelineEntry
 * @return {xhrPromise}
 */
const loadRowStoreData = (pipelineEntry) => {
  return pipelineEntry.getEntryStore().loadViaProxy(pipelineEntry.getEntryInfo().getExternalMetadataURI());
};

/**
 * The index (order) of the statuses corresponds to the pipeline return status.
 * Do not change if not changed in the RowStore/EntryStore
 * @type {string[]}
 */
const id2status = ['created', 'accepted', 'processing', 'available', 'error'];

/**
 * Transform the RowStore status value to string
 * @see id2status
 * @param {object} data
 * @return {string}
 */
const status = (data) => id2status[data.status];

/**
 *
 * @param {store/Entry} pipelineEntry
 * @return {String}
 */
const oldStatus = (pipelineEntry) =>
  pipelineEntry
    .getCachedExternalMetadata()
    .findFirstValue(
      pipelineEntry.getResourceURI(),
      'http://entrystore.org/terms/pipelineResultStatus'
    );

/**
 * Synchronizes the API status values between what was returned from RowStore and what is
 * saved in the external metadata graph
 * @param {Entry} pipelineEntry
 * @param {boolean} forceLoad
 * @return {Promise<string>}
 * @see update
 */
const syncStatus = async (pipelineEntry, forceLoad = false) => {
  const data = await loadRowStoreData(pipelineEntry);
  const newStatus = status(data);
  if (newStatus !== oldStatus(pipelineEntry)) {
    await update(pipelineEntry, data);
  }
  return newStatus;
};

const STATUS_CHECK_REPEATS = 50;
const STATUS_CHECK_DELAY_MILLIS = 500;

/**
 * Recursive function that returns only when the api status is 'available' or throws error.
 * Otherwise keeps looping every certain millis
 * @param {store/Entry} pipelineEntryURI
 * @param {number} repeat how many times should we re-try to find the API in an available status
 * @param {number} delayMillis how many millis to delay before trying again
 * @return {Promise<String>}
 * @throws
 */
const checkStatusOnRepeat = async (
  pipelineEntry,
  repeat = STATUS_CHECK_REPEATS,
  delayMillis = STATUS_CHECK_DELAY_MILLIS
) => {
  const newStatus = await syncStatus(pipelineEntry);

  switch (newStatus) {
    case 'available':
      return '';
    case 'error':
      throw Error('apiProgressError'); // reject();
    default:
      // retry checking the API after 'delayMillis'
      if (repeat > 0) {
        await promiseUtil.delay(delayMillis);
        return checkStatusOnRepeat(
          pipelineEntry,
          repeat - 1,
          undefined
        );
      }
      return 'apiProgressWarning';
  }
};

module.exports = {
  checkStatusOnRepeat
}