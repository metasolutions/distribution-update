const fs = require('fs');
const { EntryStore } = require('@entryscape/entrystore-js');
const logger = require('./logger');

const getRepositoryViaResourceURI = (resourceURI) => {
  const indexOfRepository = resourceURI.indexOf('store');
  const indexBump = 'store/'.length;
  const entrystoreURI = resourceURI.substring(0, indexOfRepository+indexBump);
  return entrystoreURI;
}

const setCookie = (repository, cookie) => {
  const entrystore = new EntryStore(repository);
  const rest = entrystore.getREST();
  rest.headers.cookie = [cookie]
  return entrystore;
}

const extractParameters = async (entrystore, resourceURI, action) =>  {
  let entry = await entrystore.getEntry(resourceURI);
  const entryId = entry.getId();
  const contextId = entry.getContext().getId();
  entry = await entrystore.getEntry(entrystore.getEntryURI(contextId, entryId));
  const entryType = entry.getMetadata().findFirstValue(null, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
  if(action==='replace' && entryType !== 'http://entryscape.com/terms/File')  {
    throw new Error('Provided entry with id ' + entryId + ' has to be of type http://entryscape.com/terms/File')
  }
  if(action === 'add' && entryType !== 'http://www.w3.org/ns/dcat#Distribution')  {
    throw new Error('Provided entry with id ' + entryId + ' has to be of type dcat:Distribution')
  }
  let distributionId = null;
  if(action === 'replace') {
    const sq = entrystore.newSolrQuery().context(contextId).rdfType("dcat:Distribution").objectUri(resourceURI);
    const distributionEntries = await sq.getEntries();
    if (distributionEntries.length>0) distributionId = distributionEntries[0].getId();
    if (!distributionId) throw new Error(`No matching id for ${resourceURI} found in access url:s of distribution ${distributionId}`);
  }
  return { entryId, contextId, distributionId };
};

const checkFile = (filePath) => {
  const stat = fs.statSync(filePath);
  if (!stat || !stat.isFile()) {
    throw Error(`The provided file "${filePath}" is not a readable file.`);
  }
}

const getFormat = (filename) => {
  const extention = filename.lastIndexOf('.') !== -1 ? filename.substring(filename.lastIndexOf('.')+1) : undefined;
  if (extention) {
    if (extention.toLowerCase() === 'csv') return 'text/csv'
    if (extention.toLowerCase() === 'json')  return 'application/json';
    if (extention.toLowerCase() === 'pdf')  return 'application/pdf';
    return 'text/plain';
  }
}

const setFileLabel = async (fileEntry, filename) => {
  logger.info('Setting label of entry to original file name');
  const entryInfo = fileEntry.getEntryInfo();
  entryInfo.setLabel(filename);
  return entryInfo.commit();
};

module.exports = {
  getRepositoryViaResourceURI,
  setCookie,
  extractParameters,
  checkFile,
  getFormat,
  setFileLabel,
}