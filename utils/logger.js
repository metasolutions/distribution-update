const log4js = require('log4js');

const setLevel = (logger) => {
  let level = 'info';
  logger.level = level;
};

const logger = log4js.getLogger();
setLevel(logger);


module.exports = logger;