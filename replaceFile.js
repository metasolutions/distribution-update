const signin = require('./utils/signin');
const { getAPIDistribution, refreshAPI } = require('./utils/api');
const fs = require('fs');

const logger = require('./utils/logger');

const {  
  getRepositoryViaResourceURI,
  setCookie,
  extractParameters,
  checkFile,
  getFormat,
  setFileLabel,
} = require('./utils/util');

const fileReplacer = async (catalogId, distributionId, fileId, filePath, format, filename, entrystore) => {
  
  if(!entrystore) entrystore = await signin();
  const baseURI = entrystore.getBaseURI();
  logger.info(`Signed in to ${baseURI}`);
  const distribution = await entrystore.getEntry(entrystore.getEntryURI(catalogId, distributionId));
  logger.info(`Fetched distribution of ${catalogId} and ${distributionId}`);
  const apiEntry = await getAPIDistribution(distribution);
  if (apiEntry && format!=='text/csv') throw new Error('Distribution has connected API, file has to be CSV formatted!');
  logger.info('Fetched api entry');

  const file = await entrystore.getEntry(entrystore.getEntryURI(catalogId, fileId));
  const resourceURI = file.getResourceURI();
  const metadata = file.getMetadata();
  logger.info(`Fetched entry for file ${resourceURI}`);

  if (filename) {
    metadata.findAndRemove(resourceURI, 'dcterms:title');
    metadata.addL(resourceURI, 'dcterms:title', filename);
  }
  metadata.findAndRemove(resourceURI, 'dcterms:format');
  await file.commitMetadata();
  
  logger.info('Uploading the file');
  const fileResource = await file.getResource();

  await fileResource.putFile(fs.createReadStream(filePath), format);

  if (filename) await setFileLabel(file, filename);

  logger.info('Updating the distribution with a new modification date.');
  const ruri = distribution.getResourceURI();
  const md = distribution.getMetadata();
  md.findAndRemove(ruri, 'dcterms:modified');
  md.addD(ruri, 'dcterms:modified', new Date().toISOString(), 'xsd:dateTime');
  await distribution.commitMetadata();

  if (apiEntry) {
    logger.info('Replacing the file of the connected API');
    await refreshAPI(apiEntry, distribution);
  } else {
    logger.info('No connected API to update');
  }
  return file.getResourceURI();
};


if (process.argv.length > 2 && process.argv.length <= 7) {
  const catalogId = process.argv[2];
  const distributionId = process.argv[3];
  const fileId = process.argv[4];
  const filePath = process.argv[5];
  const formatArg = process.argv[6];
  
  checkFile(filePath);
  const dotLocation = filePath.lastIndexOf('.');
  const filename = dotLocation !== -1 ? filePath.substring(0, dotLocation) : filePath;
  const extention = dotLocation !== -1 ? filePath.substring(dotLocation+1) : undefined;
  const format = formatArg ? formatArg
    : (extention && extention.toLowerCase() === 'csv' ? 'text/csv' : 'text/plain');
  fileReplacer(catalogId, distributionId, fileId, filePath, format, filename, null);
} else if (process.argv.length < 2) {
  logger.info("This command allows you to replace a file to a distribution if there is a connected API it will be updated as well.");
  logger.info("$> node replaceFile.js catalogId fileId pathToFile [format]");
  process.exit(1);
}

module.exports = async (cookie, filePath, filename, resourceURI) => {
  checkFile(filePath);
  const repository = getRepositoryViaResourceURI(resourceURI);
  const entrystore = setCookie(repository, cookie);
  const { entryId, contextId, distributionId } = await extractParameters(entrystore, resourceURI, 'replace');
  const format = getFormat(filename);
  logger.info(format);
  return fileReplacer(contextId, distributionId, entryId, filePath, format, filename, entrystore);
};