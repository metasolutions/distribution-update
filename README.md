# Distribution update repository

This repository contains utility methods for manipulating distributions in an data catalog maintained in EntryScapes backend EntryStore.

Before you have install dependencies via:

    npm install

Before you can use the scripts in this repository (see sections below) you have to provide a config file pointing to the EntryStore instance, username and password. You should be able to copy the file `config_example.js` to `config.js` and adapt it.

Each script below provide basic instructions via the help command:

    node scriptname.js help

Note that the scripts below canno be used to create a new distribution or initiate the API, that has to be done via the EntryScape UI. However, after a distribution is created (and potentially a connected API distribution is created as well) then subsequent updates can be done via the scripts `addFile` and `replaceAllFiles`.

## List
With this script you can easily list datasets in a catalog or distributions for a dataset. For example

    node list.js 53   // Lists all datasets in catalog 53
    node list.js 53 5 // Lists all distributions for dataset 5

## Status
With this script you can see the status of an individual distribution. Use the list script if you are unsure of which identifier to use for the distribution. For example:

    node status.js 53 5 // Shows information and list files in dataset 5
  
## addFile
This script allows you to add an individual file to a distribution. It will also update the connected API distribution if there is one.
The API will continue to work during the time the script runs and will at the end contain the extra data of the file (assuming all goes well). For example below commands adds a file to dataset 5 and also updates the API (if connected) with the new data.

    node addFile.js 53 5 data/test2.csv text/csv

## replaceFile
This script allows you to replace an individual file to a distribution. It will also update the connected API distribution if there is one.
The API will continue to work during the time the script runs and will at the end contain the replaced data of the file (assuming all goes well). For example below commands replace a file in context 53, distribution 5, and file 3. Then the API (if connected) is updated with the replaced data.

    node replaceFile.js 53 5 3 data/test2.csv text/csv

## replaceAllFiles - TODO
This script allows you to replace all files in a distribution by pointing the script to a folder with files. The file names will be used as names for the file entries, any extension such as `.csv` will be removed from the name.
If there is a connected API it will be reinitialized with the new data.

Note that replaceAllFiles will first empty the API and then add all the data again, hence you should be aware that using this script will make the API not work properly during the time the update runs.

## refreshAPI
This script allows you to refresh an API from the files already updated. In general this script should not be needed if addFile and replaceAllFiles works as expected. For example:

    node refreshAPI.js 53 5