# Adding data to a distribution with an associated API distribution

- [Two scenarios](#scenarios)
- [Prerequisities](#prerequisites)
    - [Setup in EntryScape](#setup)
    - [Locating information about the distributions](#locating)
- [Scenario 1: Adding a file](#scenario1)
    - [Step 1: Uploading a new file](#scenario1-step1)
    - [Step 2: Updating distribution metadata](#scenario1-step3)
    - [Step 3: Updating API](#scenario1-step3)
- [Scenario 2: Updating a file](#scenario2)
    - [Step 1: Updating an existing file](#scenario2-step1)
    - [Step 2: Updating distribution metadata](#scenario2-step2)
    - [Step 3: Updating API](#scenario1-step3)

<a name="scenarios"></a>
## Two scenarios
There are two different scenarios for updating the dataset with additional data.

**Scenario 1: Adding a file** - The distribution has a list of associated files to which a new file is added, e.g. there is a list of files, one per year.<br>
**Scenario 2: Updating a file** - The distribution points to a single file that is updated regularly.

But before we go into details for each of these scenarios we need to make sure we have some information about the distributions we want to update.

<a name="prerequisities"></a>
## Prerequisites

<a name="setup"></a>
### Setup in EntryScape
Let's assume that the following is the case:

1. You have access rights to a data catalog in EntryScape.
2. There is a dataset in the data catalog.
3. The dataset is represented by one or several CSV files located in a single distribution with the format set to `text/csv`.
4. The dataset has an API activated based on the CSV files. This API is handled by EntryScape, the underlying system used is called RowStore.

This setup will result in an overview of the dataset such as this one:

![overview](overview.png)

<a name="locating"></a>
### Locating information about the distributions
To be able to update the distributions we need to identify:

1. The `contextId` for the current catalog.
2. The `entryId` for the data distribution
3. The `entryId` for the data file (only for scenario 2 below).
4. The `address` to the RowStore API

You can identify both 1 & 2 by switching to the preview of the dataset and clicking on the CSV distribution (the one with the CSV file(s) in it). In the dialog that comes up, expand the "show resource information" section and observe the `resource` field. See screenshoot for where to find 1 & 2.

![](entryscape_details_dialog.png)

I the same dialog you can also find the `entryId` for the CSV file (scenario 2). It is located under "Web address for access", see screenshoot above. There may be a nice name rather than a URL there, then look into what the link points to.

To find the `address` to the RowStore API open the dialog for the other distribution (representing the API), in many cases the title of this distribution is "Auto-generated API". Locate the "Web address for access" field again but in this case the URL will look a bit different and you need to take it as a whole.

![](entryscape_API_dialog.png)

Now when we have the neccessary information, lets consider the two scenarios.

<a name="scenario1"></a>
## Scenario 1: Adding a file

The following diagram gives an overview of the procedure. Everything with a bright orange indicates that a change is happening.

![](addFile.png)

The diagram illustrates the following steps:

1. A new file is uploaded, this is done in two steps:
   1. A new Entry is created (http POST) with some metadata, e.g. a title which says "Air quality 2022".
   2. The file is pushed (http PUT) to the resource of the entry.
2. The metadata of the distribution (with title "CSV files") is updated (http PUT) in two ways:
   1. The modification date is changed.
   2. The new file is linked to via the accessURL and downloadURL properties.
3. The API (Rowstore API) is updated by:
   1. The pipeline is updated (http PUT) so it points to the right API in rowstore (URI1 in the picture) and the action is set to `append`.
   2. The pipeline is executed (http POST) on the new file.

<a name="scenario1-step1"></a>
### Step 1: Uploading a new file

To create an new file Entry you basically need to make a http POST call to the `contextId`. See [API documentation](https://entrystore.org/api/#/context/contextPost). In the response you will get the `entryId` for the newly created entry. To upload a file to this entry you need to make a http PUT call on it's resource URI. See [API documentation](https://entrystore.org/api/#/resource/resourcePut).

Lets assume that the new file we created where created with a `entryId` of `45` inside of context with a `contextId` of `37`, i.e. the full URI to the new file would be:

    https://your.domain.tld/store/37/resource/45

We will use this URI in the following steps.

<a name="scenario1-step2"></a>
### Step 2: Updating distribution metadata
To update the metadata of a distribution you need to load the metadata http GET, change it and then do 
a http PUT to push the new metadata back.

To load the metadata for the distribution you would do a request, below we assume the distribution entryId is 40:

    GET https://your.domain.tld/store/37/metadata/40

The response would be a JSON structure like the following:

    {
      "https://your.domain.tld/store/37/resource/40": {
        "http://purl.org/dc/terms/title": [{ "type": "literal", "lang": "sv", "value": "Levreskontra i CSV"}],
        "http://purl.org/dc/terms/format": [{ "type": "literal", "value": "text/csv"}],
        "http://purl.org/dc/terms/modified": [{
           "datatype": "http://www.w3.org/2001/XMLSchema#dateTime",
           "type": "literal",
           "value": "2022-06-13"
        }],
        "http://www.w3.org/ns/dcat#downloadURL": [{
          "type": "uri",
          "value": "https://your.domain.tld/store/37/resource/41"
        }],
        "http://www.w3.org/ns/dcat#accessURL": [{
          "type": "uri",
          "value": "https://your.domain.tld/store/37/resource/41"
        }],
        "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": [{
          "type": "uri",
          "value": "http://www.w3.org/ns/dcat#Distribution"
        }],
      }
    }

Let's assume the response is available in a json object in a variable `metadata`.
First, to update the modified date you do something like the following in JavaScript (which should be easily transferrable to other programming languages):

``` Javascript
const propertiesForDist = metadata["https://your.domain.tld/store/37/resource/40"];
const modifiedObj = propertiesForDist["http://purl.org/dc/terms/modified"][0];
modifiedObj.value = "2022-10-28T00:00:00"; // Or whatever date you want to set in xsd:dateTime format
```
Second, we need to add links to the new file via the properties `downloadURL` and `accessURL`. Let's add to the JavaScript code above:

``` Javascript
const accessUrlArr = propertiesForDist["http://www.w3.org/ns/dcat#accessURL"];
accessURLArr.push({"type": "uri", "value": "https://your.domain.tld/store/37/resource/45"});

const downloadUrlArr = propertiesForDist["http://www.w3.org/ns/dcat#downloadURL"];
downloadURLArr.push({"type": "uri", "value": "https://your.domain.tld/store/37/resource/45"});
```

The following JSON structure is the updated one, with changed metadata in <strong>bolded</strong> font. We will push the updated metadata back to the same address we got it from, but this time using http PUT.

<pre><code>{
  "https://your.domain.tld/store/37/resource/40": {
    "http://purl.org/dc/terms/title": [{ "type": "literal", "lang": "sv", "value": "Levreskontra i CSV"}],
    "http://purl.org/dc/terms/format": [{ "type": "literal", "value": "text/csv"}],
    "http://purl.org/dc/terms/modified": [{
        "datatype": "http://www.w3.org/2001/XMLSchema#dateTime",
        "type": "literal",
        "value": <strong>"2022-10-28"</strong>
    }],
    "http://www.w3.org/ns/dcat#downloadURL": [{
      "type": "uri",
      "value": "https://your.domain.tld/store/37/resource/41"
    }<strong>,
    {
      "type": "uri",
      "value": "https://your.domain.tld/store/37/resource/45"
    }</strong>],
    "http://www.w3.org/ns/dcat#accessURL": [{
      "type": "uri",
      "value": "https://your.domain.tld/store/37/resource/41"
    }<strong>,
    {
      "type": "uri",
      "value": "https://your.domain.tld/store/37/resource/45"
    }</strong>],
    "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": [{
      "type": "uri",
      "value": "http://www.w3.org/ns/dcat#Distribution"
    }],
  }
}</code></pre>

<a name="scenario1-step3"></a>
#### Step 3: Updating the API
We will reuse an existing pipeline entry to append the data to the API. But first we need to make sure the pipeline is configured correctly. For this we need to tweak the resource of the pipeline, not it's metadata. The pipelines `entryId` is always `rowstorePipeline` and you get it by doing:

    GET https://your.domain.tld/store/37/resource/rowstorePipeline

Before we look at the response we take a brief look at what a pipeline is from a conceptual perspective. A pipeline consists of a list of transforms each which have a type and a set of arguments. Each argument is represented with a key and a value. I.e. a conceptual representation of the pipeline we get back from the request is:

<pre><code>transform1:
   type: rowstore
   argument1:
     key: action
     value: <strong>replace</strong>
   argument2:
     key: datasetURL
     value: <strong>https://your.domain.tld/rowstore/dataset/8d6f2755-693f-4b07-aded-d995304d0aec</strong></code></pre>

Where the parts we need to change is bolded.

The response we get back in RDF is slighlty more complicated (for various reasons that is out of scope of this text):

<pre><code>{
  "https://your.domain.tld/store/37/resource/rowstorePipeline": {
    "http://entrystore.org/terms/transform": [{ "type": "bnode", "value": "_:node1ggmhairnx1" }]
  },
  "_:node1ggmhairnx1": {
    "http://entrystore.org/terms/transformType": [{ "type": "literal", "value": "rowstore" }],
    "http://entrystore.org/terms/transformArgument": [
      { "type": "bnode", "value": "_:node1ggmhairnx2" },
      { "type": "bnode", "value": "_:node1ggmhairnx3"}
    ]
  },
  "_:node1ggmhairnx2": {
    "http://entrystore.org/terms/transformArgumentKey": [{"type": "literal", "value": "action"}],
    "http://entrystore.org/terms/transformArgumentValue": [
      {
        "type": "literal",
        "value": <strong>"replace"</strong> 
      }
    ]
  },
  "_:node1ggmhairnx3": {
    "http://entrystore.org/terms/transformArgumentKey": [{ "type": "literal", "value": "datasetURL" }],
    "http://entrystore.org/terms/transformArgumentValue": [
     {
       "type": "literal",
       "value": <strong>"https://your.domain.tld/rowstore/dataset/8d6f2755-693f-4b07-aded-d995304d0aec"</strong>
     }
    ]
  }
}</code></pre>

The correct way to manipulate this expression is (in JavaScript) is shown below.
We assume the response of the GET is available in the variable `resource` and the correct address to the API in rowstore is available in the `address` variable (called URI1 in the diagrams).

``` JavaScript
// Constants to simplify code below
const transformProp = "http://entrystore.org/terms/transform";
const argumentProp = "http://entrystore.org/terms/transformArgument";
const keyProp = "http://entrystore.org/terms/transformArgumentKey";
const valueProp = "http://entrystore.org/terms/transformArgumentValue";

// Find all transform arguments for the only transform in the pipeline
const pipelineURI = "https://your.domain.tld/store/37/resource/rowstorePipeline";
const transform1 = resource[pipelineURI][transformProp].value;
const transformArgs = resource[tranforms1][argumentProp].map(o => o.value);

// Identify which argument is for the action and which is for the datasetURL.
const actionArg = resource[transformArgs[0]][keyProp].value === 'action ? transformArgs[0] : transformArgs[1]:
const datasetURLArg = transformArgs[0] === actionArg ? transformArgs[1] : transformArgs[0];

// Make sure the pipeline has the action "append"
resource[actionArg][valueProp].value = "append";

// Make sure we point to the right API in rowstore
resource[datasetURLArg][valueProp].value = address;
```

Now when we made all neccessary changes to the pipeline we update it via http PUT to the same address we got it from.

Finally, we need to execute the pipeline on the new file. Executing a pipeline is done by making a POST on a generic resource inside of the context:

```
POST https://your.domain.tld/store/37/execute
{
  "pipeline": "https://your.domain.tld/store/37/entry/rowstorePipeline",
  "source": "https://your.domain.tld/store/37/entry/45"
}
```

**Note 1** we left out a lot of the http request headers, e.g. the cookie authorization.<br>
**Note 2** the URIs to the pipeline and the new file have `entry` in the path, not `resource` this is as it should.

Importing the data into the API is an asynchronous operation. To see if the API is ready or not you can make a request to the datasetURI with the `/info` appended to it, e.g:

    GET https://your.domain.tld/rowstore/dataset/8d6f2755-693f-4b07-aded-d995304d0aec/info

The respons will look something like:

``` JSON
{
  "@context": "https://entrystore.org/rowstore/",
  "@id": "https://your.domain.tld/rowstore/dataset/8d6f2755-693f-4b07-aded-d995304d0aec",
  "identifier": "8d6f2755-693f-4b07-aded-d995304d0aec",
  "aliases": [],
  "rowcount": 17,
  "created": "2022-10-13T15:48:05+0200",
  "columnnames": [ "col1", "col2", "col3" ],
  "status": 3
}
```
Read more in the [rowstore documentation](https://bitbucket.org/metasolutions/rowstore/src/master/) on various aspects of the answer, including what the status flag represents.

<a name="scenario2"></a>
## Scenario 2: Updating a file

The following diagram gives an overview of the procedure. Everything with a bright orange indicates that a change is happening.

![](updateFile.png)

The diagram illustrates the following steps:

1. The existing file is uploaded, this is done the file is pushed (http PUT) to the resource of the file entry.
2. The metadata of the distribution (with title "CSV files") is updated (http PUT) with the new modification date.
3. The API (Rowstore API) is updated by:
    1. The pipeline is updated (http PUT) so it points to the right API (URI1 in the picture) and the action is set to `replace`.
    2. The pipeline is executed (http POST) on the new file.

In this scenario we must have the reference to a data file in the form of an entry already. We will assume in the following that it's `entryId` is `41` (and the `contextId` is still `37` as in the add file scenario).

<a name="scenario2-step1"></a>
### Step 1: Updating an existing file
We need to upload the new file, that is done via a simple http PUT on the entry resource, i.e. to the address `https://your.domain.tld/store/37/resource/41`. In the diagram representation the title in the metadata of the file is also updated (e.g. changed to "2015-2022"), this is not strictly neccessary and was only made in the diagram to indicate that the file is changed. A better approach is to have a title that never need change, e.g. just "Air quality".

<a name="scenario2-step2"></a>
### Step 2: Updating distribution metadata
This step is a simpler version of what we had scenario 1 as we only update the modification date, not add any links.

<a name="scenario2-step3"></a>
### Step 3: Updating API
This step is close to being identical to what was described in scenario 1. The only difference is that the action should be `replace` instead of `append`.
