const signin = require('./utils/signin');
const { EntryStoreUtil } = require('@entryscape/entrystore-js');
const { getAPIDistribution } = require('./utils/api');

if (process.argv.length < 4 || process.argv[3].indexOf('help') !== -1) {
  console.log("This command allows you to see the status of a distribution including it's files and if there is a connected API.");
  console.log("$> node status.js catalogId distributionId");
  console.log("\nWhere catalogId is the contextId and distributionId is the entryId in EntryScape.");
  process.exit(1);
}
const catalogId = process.argv[2];
const distributionId = process.argv[3];

signin().then((es) => {
  const esu = new EntryStoreUtil(es);

  es.getEntry(es.getEntryURI(catalogId, distributionId)).then(async distribution => {
    const ruri = distribution.getResourceURI();
    const md = distribution.getMetadata();
    const title = md.findFirstValue(ruri, 'dcterms:title') || '';
    const format = md.findFirstValue(ruri, 'dcterms:format') || '';
    const access = md.findFirstValue(ruri, 'dcat:accessURL') || '';
    console.log(`Title: ${title}`);
    console.log(`Format: ${format}`);
    console.log(`Access: ${access}`);

    const apiEntry = await getAPIDistribution(distribution);
    if (apiEntry) {
      console.log(`Connected API distribution has id: ${apiEntry.getId()}`);
    } else {
      console.log('No connected API distribution');
    }

    console.log('\nFiles in distribution');
    console.log('---------------------');
    const fileURIs = md.find(ruri, 'dcat:downloadURL').map(stmt => stmt.getValue());
    esu.loadEntriesByResourceURIs(fileURIs, catalogId).then(fileEntries => {
      fileEntries.forEach(fileEntry => {
        const title = fileEntry.getMetadata().findFirstValue(fileEntry.getResourceURI(), 'dcterms:title');
        const format = fileEntry.getMetadata().findFirstValue(fileEntry.getResourceURI(), 'dcterms:title');
        console.log(`${fileEntry.getId()}\t${title}    ${format}`);
      });
    })
  });
});