const signin = require('./utils/signin');
const { refreshAPI, getAPIDistribution } = require('./utils/api');
const { EntryStoreUtil } = require('@entryscape/entrystore-js');
const fs = require('fs');

const logger = require('./utils/logger');

if (process.argv.length < 4 || process.argv[3].indexOf('help') !== -1) {
  logger.info("This command allows you to refresh the API connected to the distribution.");
  logger.info("$> node refreshAPI.js catalogId distributionId ");
  process.exit(1);
}
const catalogId = process.argv[2];
const distributionId = process.argv[3];

signin().then((es) => {
  const esu = new EntryStoreUtil(es);

  return es.getEntry(es.getEntryURI(catalogId, distributionId)).then(async sourceDistribution => {
    const apiDistribution = await getAPIDistribution(sourceDistribution);
    await refreshAPI(apiDistribution, sourceDistribution);
    return true;
  });
});